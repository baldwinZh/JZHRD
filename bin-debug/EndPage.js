// 结算界面
// author: Baldwin
// date: 20180601
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var EndPage = (function (_super) {
    __extends(EndPage, _super);
    function EndPage() {
        var _this = _super.call(this) || this;
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAddedToStage, _this);
        return _this;
    }
    EndPage.prototype.onAddedToStage = function (event) {
        this.removeEventListener(egret.Event.ADDED_TO_STAGE, this.onAddedToStage, this);
    };
    return EndPage;
}(egret.Sprite));
__reflect(EndPage.prototype, "EndPage");
