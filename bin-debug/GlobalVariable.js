var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var GlobalVariable = (function () {
    function GlobalVariable() {
    }
    GlobalVariable._DEBUG = false; // 调式开关
    GlobalVariable._WITHAD = false; // 广告开关
    GlobalVariable._stageWidth = 640; //egret.MainContext.instance.stage.stageWidth ; // 舞台宽度
    GlobalVariable._stageHeight = 1136; //egret.MainContext.instance.stage.stageHeight; // 舞台高度
    GlobalVariable._GeneralFontColor = 0xA0522D;
    GlobalVariable._ButtonColor = 0xA0522D;
    GlobalVariable._ButtonFontColor = 0xffffff;
    GlobalVariable._threeLevelTotalTime = 60;
    GlobalVariable._fourLevelTotalTime = 120;
    GlobalVariable._fiveLevelTotalTime = 180;
    return GlobalVariable;
}());
__reflect(GlobalVariable.prototype, "GlobalVariable");
var GameLevel;
(function (GameLevel) {
    GameLevel[GameLevel["THREE"] = 0] = "THREE";
    GameLevel[GameLevel["FOUR"] = 1] = "FOUR";
    GameLevel[GameLevel["FIVE"] = 2] = "FIVE";
})(GameLevel || (GameLevel = {}));
;
