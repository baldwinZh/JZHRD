// 游戏组件
// author: baldwin
// date: 20180602
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var GameComponent = (function (_super) {
    __extends(GameComponent, _super);
    function GameComponent(x, y, stageWidth, stageHeight, level) {
        var _this = _super.call(this) || this;
        _this._box_num = 3;
        _this._gameLevel = level;
        if (_this._gameLevel == GameLevel.THREE) {
            _this._box_num = 3;
            _this._textArr = [1, 2, 3, 4, 5, 6, 7, 8];
        }
        else if (_this._gameLevel == GameLevel.FOUR) {
            _this._box_num = 4;
            _this._textArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];
        }
        else if (_this._gameLevel == GameLevel.FIVE) {
            _this._box_num = 5;
            _this._textArr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24];
        }
        _this._stageWidth = stageWidth;
        _this._stageHeight = stageHeight;
        _this._x = x;
        _this._y = y;
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAddedToStage, _this);
        return _this;
    }
    GameComponent.prototype.onAddedToStage = function (event) {
        this.removeEventListener(egret.Event.ADDED_TO_STAGE, this.onAddedToStage, this);
        this.Init();
    };
    GameComponent.prototype.Init = function () {
        this._backGround = Functions.createBitmapByName("gameback_jpg");
        this._backGround.width = this._stageWidth * 0.94;
        this._backGround.height = this._stageWidth * 0.94;
        this._backGround.x = this._x;
        this._backGround.y = this._y;
        this.addChild(this._backGround);
        var totalW = this._backGround.width;
        var totalH = this._backGround.height;
        var boxWidth = totalW / (0.1 + 1.1 * this._box_num);
        var space = boxWidth / 10;
        var x = this._x + space;
        var y = this._y + space;
        var arrIndex = 0;
        this._textArr.sort(function () { return 0.5 - Math.random(); });
        console.log("_textArr, ", this._textArr);
        // 初始化数字方块
        this._grid_array = new Array(this._box_num);
        for (var row = 0; row < this._box_num; row++) {
            var x1 = x;
            this._grid_array[row] = new Array(this._box_num);
            for (var column = 0; column < this._box_num; column++) {
                if (row == this._box_num - 1 && column == this._box_num - 1) {
                    break;
                }
                this._grid_array[row][column] = new Box(this._gameLevel, x1, y, boxWidth, boxWidth, this._textArr[arrIndex]);
                this.addChild(this._grid_array[row][column]);
                this._grid_array[row][column].touchEnabled = true;
                arrIndex++;
                x1 = x1 + boxWidth + space;
            }
            y = y + boxWidth + space;
        }
    };
    return GameComponent;
}(egret.Sprite));
__reflect(GameComponent.prototype, "GameComponent");
