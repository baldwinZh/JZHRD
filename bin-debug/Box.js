// Author: baldwin
// Date: 20180602
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var __extends = this && this.__extends || function __extends(t, e) { 
 function r() { 
 this.constructor = t;
}
for (var i in e) e.hasOwnProperty(i) && (t[i] = e[i]);
r.prototype = e.prototype, t.prototype = new r();
};
var Box = (function (_super) {
    __extends(Box, _super);
    function Box(level, x, y, w, h, text) {
        var _this = _super.call(this) || this;
        _this._level = level;
        _this._x = x;
        _this._y = y;
        _this._w = w;
        _this._h = h;
        _this._text = text;
        if (_this._level == GameLevel.THREE) {
            _this._textSize = 70;
        }
        else if (_this._level == GameLevel.FOUR) {
            _this._textSize = 50;
        }
        else if (_this._level == GameLevel.FIVE) {
            _this._textSize = 40;
        }
        _this.addEventListener(egret.Event.ADDED_TO_STAGE, _this.onAddedToStage, _this);
        return _this;
    }
    Box.prototype.onAddedToStage = function (event) {
        this.removeEventListener(egret.Event.ADDED_TO_STAGE, this.onAddedToStage, this);
        this.init();
    };
    Box.prototype.init = function () {
        this._backGround = Functions.createBitmapByName("box_jpg");
        this._backGround.width = this._w;
        this._backGround.height = this._h;
        this._backGround.x = this._x;
        this._backGround.y = this._y;
        this.addChild(this._backGround);
        this._textText = new egret.TextField();
        this._textText.text = this._text.toString();
        this._textText.textColor = GlobalVariable._ButtonFontColor;
        this._textText.fontFamily = "微软雅黑";
        this._textText.width = this._w;
        this._textText.height = this._h;
        this._textText.verticalAlign = egret.VerticalAlign.MIDDLE;
        this._textText.textAlign = egret.HorizontalAlign.CENTER;
        this._textText.x = this._x;
        this._textText.y = this._y;
        this._textText.size = this._textSize;
        this._textText.bold = true;
        this.addChild(this._textText);
    };
    return Box;
}(egret.Sprite));
__reflect(Box.prototype, "Box");
