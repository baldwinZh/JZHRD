// author: baldwin
// date: 20180602
var __reflect = (this && this.__reflect) || function (p, c, t) {
    p.__class__ = c, t ? t.push(c) : t = [c], p.__types__ = p.__types__ ? t.concat(p.__types__) : t;
};
var PageManager = (function () {
    function PageManager() {
        this._startPage = new StartPage();
    }
    PageManager.prototype.SetMain = function (main) {
        this._main = main;
    };
    PageManager.GetInstance = function () {
        if (PageManager._obj == null) {
            PageManager._obj = new PageManager();
        }
        return PageManager._obj;
    };
    PageManager.prototype.Handle = function (t, param) {
        switch (t) {
            case PageHandleType.START_GAME: {
                this._main.addChild(this._startPage);
                break;
            }
            case PageHandleType.START_GAME_TO_MAIN_SECENE: {
                if (param == null || param == undefined) {
                    return;
                }
                this._main.removeChild(this._startPage);
                this._mainSecene = new MainScene(param.stageWidth, param.stageHeight, param.level, param._totalTime);
                this._main.addChild(this._mainSecene);
                break;
            }
            case PageHandleType.MAIN_SECENE_TO_START_PAGE: {
                this._main.removeChild(this._mainSecene);
                this._main.addChild(this._startPage);
                break;
            }
        }
    };
    PageManager._obj = null;
    return PageManager;
}());
__reflect(PageManager.prototype, "PageManager");
var PageHandleParameters = (function () {
    function PageHandleParameters() {
    }
    return PageHandleParameters;
}());
__reflect(PageHandleParameters.prototype, "PageHandleParameters");
var PageHandleType;
(function (PageHandleType) {
    PageHandleType[PageHandleType["START_GAME"] = 0] = "START_GAME";
    PageHandleType[PageHandleType["START_GAME_TO_MAIN_SECENE"] = 1] = "START_GAME_TO_MAIN_SECENE";
    PageHandleType[PageHandleType["MAIN_SECENE_TO_START_PAGE"] = 2] = "MAIN_SECENE_TO_START_PAGE";
})(PageHandleType || (PageHandleType = {}));
;
